package devil.crud.error;

import devil.crud.controller.response.Response;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class HandleException {
    @ExceptionHandler(BadRequestException.class)
    public @ResponseBody ResponseEntity<Response<Object>> handleBadRequestException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Response<Object> dtoResult = new Response<Object>();
        dtoResult.setCode("400");
        dtoResult.setMessage(ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(headers).body(dtoResult);
    }
    @ExceptionHandler(NotFoundException.class)
    public @ResponseBody ResponseEntity<Response<Object>> handleNotFoundException(Exception ex, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        Response<Object> dtoResult = new Response<Object>();
        dtoResult.setCode("404");
        dtoResult.setMessage(ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(headers).body(dtoResult);
    }
}
