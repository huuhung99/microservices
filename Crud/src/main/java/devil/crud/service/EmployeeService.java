package devil.crud.service;

import devil.crud.controller.request.EmployeeRequest;
import devil.crud.dto.EmployeeDto;

import java.util.List;

public interface EmployeeService {
    List<EmployeeDto> getAll();
    EmployeeDto findById(Long id);
    EmployeeDto createOrUpdate(EmployeeRequest request);
    void deleteById(Long id);
}
