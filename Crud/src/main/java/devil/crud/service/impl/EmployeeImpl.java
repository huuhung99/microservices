package devil.crud.service.impl;

import devil.crud.controller.request.EmployeeRequest;
import devil.crud.dto.EmployeeDto;
import devil.crud.error.BadRequestException;
import devil.crud.error.NotFoundException;
import devil.crud.model.Employee;
import devil.crud.repository.EmployeeRepository;
import devil.crud.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class EmployeeImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Override
    public List<EmployeeDto> getAll() {
        return employeeRepository.findAll().stream().map(this::toDto).collect(Collectors.toList());
    }

    @Override
    public EmployeeDto findById(Long id) {
        Optional<Employee> op = employeeRepository.findById(id);
        if(!op.isPresent()){
            throw new NotFoundException("Employee id does not exit!");
        }
        return this.toDto(op.get());
    }

    @Override
    public EmployeeDto createOrUpdate(EmployeeRequest request) {
        Employee employee=new Employee();
        BeanUtils.copyProperties(request,employee);
        if(request.getId()!=null){
            Optional<Employee> op = employeeRepository.findById(request.getId());
            if(!op.isPresent()){
                throw new BadRequestException("Employee id does not exit!");
            }
        }
        return toDto(employeeRepository.save(employee));
    }

    @Override
    public void deleteById(Long id) {
        Optional<Employee> op = employeeRepository.findById(id);
        if(!op.isPresent()){
            throw new NotFoundException("Employee id does not exit!");
        }
        employeeRepository.deleteById(id);
    }
    private EmployeeDto toDto(Employee employee){
        EmployeeDto employeeDto=new EmployeeDto();
        BeanUtils.copyProperties(employee,employeeDto);
        return employeeDto;
    }
}
