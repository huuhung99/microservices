package devil.crud.controller.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeRequest {
    private Long id;
    @JsonProperty("last_name")
    @NotNull
    private String lastName;
    @JsonProperty("first_name")
    @NotNull
    private String firstName;
    private String email;
    private String phone;
    @NotNull
    private String sex;
    private String address;
    @JsonProperty("birth_day")
    private Instant birthDay;
}
