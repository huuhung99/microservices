package devil.crud.controller.api;

import devil.crud.controller.request.EmployeeRequest;
import devil.crud.controller.response.Response;
import devil.crud.dto.EmployeeDto;
import devil.crud.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;
    @GetMapping
    public ResponseEntity<Response<EmployeeDto>> getAll(){
        List<EmployeeDto> employeeDtos = employeeService.getAll();
        Response response=new Response("200","OK",employeeDtos);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Response<EmployeeDto>> findById(@PathVariable Long id){
        EmployeeDto employeeDto = employeeService.findById(id);
        Response response=new Response("200","OK",employeeDto);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    @PostMapping
    public  ResponseEntity<Response<EmployeeDto>> createOfUpdate(@RequestBody EmployeeRequest request){
        EmployeeDto employeeDto = employeeService.createOrUpdate(request);
        Response response=new Response("201","Create",employeeDto);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Response<Object>> deleteById(@PathVariable Long id){
        employeeService.deleteById(id);
        Response response=new Response("200","Delete",null);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
