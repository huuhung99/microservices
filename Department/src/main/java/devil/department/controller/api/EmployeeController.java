package devil.department.controller.api;

import devil.department.controller.request.DepartmentRequest;
import devil.department.controller.response.Response;
import devil.department.dto.DepartmentDto;
import devil.department.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/department")
public class EmployeeController {
    @Autowired
    private DepartmentService departmentService;
    @GetMapping
    public ResponseEntity<Response<DepartmentDto>> getAll(){
        List<DepartmentDto> departmentDtos = departmentService.getAll();
        Response response=new Response("200","OK",departmentDtos);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Response<DepartmentDto>> findById(@PathVariable Long id){
        DepartmentDto departmentDto = departmentService.findById(id);
        Response response=new Response("200","OK",departmentDto);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    @PostMapping
    public  ResponseEntity<Response<DepartmentDto>> createOfUpdate(@RequestBody DepartmentRequest request){
        DepartmentDto departmentDto = departmentService.createOrUpdate(request);
        Response response=new Response("201","Create",departmentDto);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Response<Object>> deleteById(@PathVariable Long id){
        departmentService.deleteById(id);
        Response response=new Response("200","Delete",null);
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }
}
