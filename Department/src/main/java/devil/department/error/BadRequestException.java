package devil.department.error;

public class BadRequestException extends RuntimeException{
    private static final long serialVersionUID = 2l;

    public BadRequestException(String err) {
        super(err);
    }
}
