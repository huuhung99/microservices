package devil.department.error;

public class NotFoundException extends RuntimeException{
    private static final long serialVersionUID = -99l;

    public NotFoundException(String err) {
        super(err);
    }
}
