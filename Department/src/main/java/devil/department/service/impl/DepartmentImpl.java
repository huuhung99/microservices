package devil.department.service.impl;

import devil.department.controller.request.DepartmentRequest;
import devil.department.dto.DepartmentDto;
import devil.department.error.BadRequestException;
import devil.department.error.NotFoundException;
import devil.department.model.Department;
import devil.department.repository.DepartmentRepository;
import devil.department.service.DepartmentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class DepartmentImpl implements DepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;
    @Override
    public List<DepartmentDto> getAll() {
        return departmentRepository.findAll().stream().map(this::toDto).collect(Collectors.toList());
    }

    @Override
    public DepartmentDto findById(Long id) {
        Optional<Department> op = departmentRepository.findById(id);
        if(!op.isPresent()){
            throw new NotFoundException("Department id does not exit!");
        }
        return this.toDto(op.get());
    }

    @Override
    public DepartmentDto createOrUpdate(DepartmentRequest request) {
        Department department=new Department();
        BeanUtils.copyProperties(request,department);
        if(request.getId()!=null){
            Optional<Department> op = departmentRepository.findById(request.getId());
            if(!op.isPresent()){
                throw new BadRequestException("Department id does not exit!");
            }
        }
        return toDto(departmentRepository.save(department));
    }

    @Override
    public void deleteById(Long id) {
        Optional<Department> op = departmentRepository.findById(id);
        if(!op.isPresent()){
            throw new NotFoundException("Department id does not exit!");
        }
        departmentRepository.deleteById(id);
    }
    private DepartmentDto toDto(Department department){
        DepartmentDto departmentDto=new DepartmentDto();
        BeanUtils.copyProperties(department,departmentDto);
        return departmentDto;
    }
}
