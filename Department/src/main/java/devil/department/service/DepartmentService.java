package devil.department.service;


import devil.department.controller.request.DepartmentRequest;
import devil.department.dto.DepartmentDto;

import java.util.List;

public interface DepartmentService {
    List<DepartmentDto> getAll();
    DepartmentDto findById(Long id);
    DepartmentDto createOrUpdate(DepartmentRequest request);
    void deleteById(Long id);
}
